<h1 align="center">Lity for Desktop</h1>

<img width="1392" alt="Screenshot 2022-08-14 at 14 46 14" src="https://user-images.githubusercontent.com/12971934/184537544-18c0929a-b0f9-4389-8e3a-0de4b338455d.png">

Graph-based document editor with collaborative features

* Create nodes by mouse, keyboard shortcuts or using markup (like @ and soon #)
* Share your creation and watch others add their own ideas

[![app-icon copy](https://user-images.githubusercontent.com/12971934/184537872-3aebba14-79db-4bae-a7a4-6b2cb4bf4131.png)](https://app.lity.cc)

### [From my blog](https://janis.io/journal/causality-and-the-future)

### [Website](https://lity.cc)

Sincerely 

Janis Jendraß
